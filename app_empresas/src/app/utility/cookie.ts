import { toSeconds } from "./config";

export function setCookie(name, value, expiry) {
    if(getCookie(name)){
        eraseCookie(name);
    }
    let maxAge = "";
    if (expiry) {
      maxAge = "; Max-Age=" + toSeconds(expiry);
    }
    document.cookie = name + "=" + (value || "") + maxAge + "; path=/";
  }
  
  export function getCookie(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(";");
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === " ") c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }
  
  export function eraseCookie(name) {
    document.cookie = name + "=; Max-Age=-99999999;";
  }
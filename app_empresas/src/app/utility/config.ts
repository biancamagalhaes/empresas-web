import { getCookie } from "./cookie";

export const headers = {
    'Content-Type': 'application/json',
    'access-token': getCookie('acessToken'),
    'client': getCookie('client'),
    'uid': getCookie('uid'),
}

export const toSeconds = (nanoseconds: number) => {
    return nanoseconds/1000000;
}
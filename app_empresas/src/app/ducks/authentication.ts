import { hen, Hen } from "../utility/createRedux";
import { createSelector } from "reselect";
import { RootState } from "../ducks/state";
import { ThunkAction } from "redux-thunk";
import { setCookie } from "../utility/cookie";
import axios from "axios";

export type Investor = {
    id: number;
    investor_name: string;
    email: string;
    city: string;
    country: string;
    balance: number;
    photo: null;
    portfolio: {
        enterprises_number: number;
        enterprises: Array<any>
    };
    portfolio_value: number;
    first_access: boolean;
    super_angel: boolean
};

export interface AuthenticationState {
    investor: Investor;
    loading: boolean;
    accessToken: string;
    client: string;
    uid: string;
}

export type InitialState = AuthenticationState;

const initialState: InitialState = {
    investor: {} as Investor,
    loading: false,
    accessToken: '',
    client: '',
    uid: ''
};

const mainSelector = (state: RootState) => state.authentication;

export const getInvestor = createSelector(mainSelector, state => {
    return {
        loading: state.loading,
        investor: state.investor,

    };
});

export const getAuthHeader = createSelector(mainSelector, state => {
    return {
        accessToken: state.accessToken,
        client: state.client,
        uid: state.uid
    };
});

class EditorReactions extends Hen<InitialState> {
    setLoading(a: boolean) {
        this.state.loading = a;
    }

    setInvestor(i: Investor) {
        this.state.investor = i;
    }

    setAuthHeaders(a: string, c: string, u: string, e: number) {
        setCookie('acessToken', a, e);
        setCookie('client', c, e);
        setCookie('uid', u, e);
        this.state.accessToken = a;
        this.state.client = c;
        this.state.uid = u;
    }
}

//Reducers
export const [menuReducer, actions] = hen(new EditorReactions(initialState));
export default menuReducer;

export function SingIn(
    data: { login: string, password: string }
): ThunkAction<Promise<void>, RootState, any, any> {
    return async (dispatch, getState) => {
        dispatch(actions.setLoading(true));
        return axios
            .post(`${process.env.REACT_APP_API}/api/${process.env.REACT_APP_API_VERSION}/users/auth/sign_in`, { email: data.login, password: data.password })
            .then((r: any) => {
                dispatch(actions.setInvestor(r.data.investor));
                dispatch(actions.setAuthHeaders(r.headers["access-token"], r.headers.client, r.headers.uid, r.headers.expiry))
                dispatch(actions.setLoading(false));
                return r.data.investor;
            })
            .catch(e => {
                console.error(e);
                dispatch(actions.setLoading(false));
                return null;
            });
    };
}


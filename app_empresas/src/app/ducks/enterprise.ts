import { hen, Hen } from "../utility/createRedux";
import { createSelector } from "reselect";
import { RootState } from "./state";
import { ThunkAction } from "redux-thunk";
import {headers} from "../utility/config";
import axios from "axios";

export type Enterprise = {
    id: number,
    email_enterprise: string,
    facebook: string,
    twitter: string,
    linkedin: string,
    phone: string,
    own_enterprise: boolean,
    enterprise_name: string,
    photo: string,
    description: string,
    city: string,
    country: string,
    value: number,
    share_price: number,
    enterprise_type: {
        id: number,
        enterprise_type_name: string
    }
};

export interface EnterpriseState {
    enterprise: Enterprise;
    searchedEnterprises: Array<Enterprise>;
    searchedWord: string;
    loading: boolean;
}

export type InitialState = EnterpriseState;

const initialState: InitialState = {
    searchedEnterprises: undefined as any,
    enterprise: {} as Enterprise,
    loading: false,
    searchedWord: ""
};

const mainSelector = (state: RootState) => state.enterprise;

export const getEnterprise = createSelector(mainSelector, state => {
    return {
        loading: state.loading,
        enterprise: state.enterprise,
    };
});

export const getSearchedEnterprises = createSelector(mainSelector, state => {
    return {
        loading: state.loading,
        searchedEnterprises: state.searchedEnterprises,
        searchedWord: state.searchedWord
    };
});

class EditorReactions extends Hen<InitialState> {
    setLoading(a: boolean) {
        this.state.loading = a;
    }

    setEnterprise(e: Enterprise) {
        this.state.enterprise = e;
    }

    setEnterprises(e: Array<Enterprise>) {
        this.state.searchedEnterprises = e;
    }

    setSearchedWord(txt: string) {
        this.state.searchedWord = txt;
    }
}

//Reducers
export const [menuReducer, actions] = hen(new EditorReactions(initialState));
export default menuReducer;

export function SearchEnterprises(
    value
): ThunkAction<Promise<void>, RootState, any, any> {
    return async (dispatch, getState) => {
        dispatch(actions.setLoading(true));
        return axios
            .get(`${process.env.REACT_APP_API}/api/${process.env.REACT_APP_API_VERSION}/enterprises?name=${value}`,{headers: headers})
            .then((r: any) => {
                dispatch(actions.setSearchedWord(value));
                dispatch(actions.setEnterprises(r.data.enterprises));
                dispatch(actions.setLoading(false));
            })
            .catch(e => {
                console.error(e);
                dispatch(actions.setLoading(false));
            });
    };
}

export function ViewEnterprise(
    enterprise: Enterprise
): ThunkAction<Promise<void>, RootState, any, any> {
    return async (dispatch, getState) => {
        dispatch(actions.setEnterprise(enterprise));
    };
}

export function FindEnterprise(
    id: string
): ThunkAction<Promise<void>, RootState, any, any> {
    return async (dispatch, getState) => {
        dispatch(actions.setLoading(true));
        return axios
            .get(`${process.env.REACT_APP_API}/api/${process.env.REACT_APP_API_VERSION}/enterprises/${id}`,{headers: headers})
            .then((r: any) => {
                dispatch(actions.setEnterprise(r.data.enterprise));
                dispatch(actions.setLoading(false));
            })
            .catch(e => {
                console.error(e);
                dispatch(actions.setLoading(false));
            });
    };
}

export function CleanSearch(): ThunkAction<Promise<void>, RootState, any, any> {
    return async (dispatch, getState) => {
        dispatch(actions.setSearchedWord(""));
        dispatch(actions.setEnterprises(undefined as any));
    };
}


import { combineReducers, Reducer } from "redux";
import { RootState } from "./state";
import Authentication from "./authentication";
import Enterprise from "./enterprise";
import axios from "axios";
import { connectRouter } from "connected-react-router";
import { createBrowserHistory } from "history";

export const history = createBrowserHistory();

export interface Action {
  type: string;
  payload: any;
}

export const reducers: Reducer<RootState> = combineReducers<RootState, any>({
  router: connectRouter(history),
  authentication: Authentication,
  enterprise: Enterprise
});

export const rootReducer = (state: RootState, action: any) => {
  return reducers(state, action);
};

axios.defaults.headers = {
  "Content-Type": "application/json",
};

export type Error = any;

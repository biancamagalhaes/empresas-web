import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin: -8px;
  background-color: #eeecdb;
`;

export const Logo = styled.img`
  width: 14.64rem;
  height: 3.563rem;
  object-fit: contain;
  flex: 1;
`;

export const Header = styled.div`
  display: flex;
  flex: 0.15;
  max-height: 4.438rem;
  padding: 1.063rem 2.5rem 1.625rem;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background-image: linear-gradient(180deg, #ee4c77 51%, #0d0430 264%);;
`;

export const Text = styled.h3`
  flex: 1;
  display: flex;
  font-family: 'Roboto';
  font-size: 2rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.22;
  letter-spacing: -0.45px;
  text-align: center;
  align-self: center;
  align-items: center;
  color: #383743;
`;

export const ListCards = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  margin-bottom: 2.75rem;
`;

export const EmptySearchText = styled.h1`
  font-family: Roboto;
  font-size: 2.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #b5b4b4;
  margin-top: 18rem;
`;


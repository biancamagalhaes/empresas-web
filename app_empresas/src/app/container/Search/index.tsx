import {Container, Header, ListCards, EmptySearchText} from "./style";
import {withRouter} from 'react-router-dom'
import SearchBar from "../../components/SearchBar";
import CardEnterprise from  "../../components/CardEnterprise";
import Loading from "../../components/Loading";
import { connect } from 'react-redux';
import {Enterprise, getSearchedEnterprises, 
  SearchEnterprises, ViewEnterprise, CleanSearch} from "../../ducks/enterprise";

export function Search(props: {searchEnterprise: (value: string) => void, 
  searchedEnterprises: Array<Enterprise>, history: any, viewEnterprise: (enterprise: Enterprise) => void, 
  cleanEnterprise: () => void, saveSearchedWord: () => void, searchedWord: string, loading: boolean}) {

  const {viewEnterprise, history, searchedEnterprises, searchEnterprise, cleanEnterprise, 
    searchedWord, loading} = props;

  const onClickCard = (enterprise) => {
    viewEnterprise(enterprise);
    history.push({pathname: `/enterprise/${enterprise.id}`});
  }

  return (
    <Container>
        <Header>
          <SearchBar 
            search={(value: string) => searchEnterprise(value)} 
            searchedWord={searchedWord}
            cleanEnterprises={cleanEnterprise} 
            type={"text"}/>
        </Header>
        <ListCards>
          {searchedEnterprises?.length > 0 ?
            searchedEnterprises?.map((item: any, index: number) => 
              <CardEnterprise 
                key={index}
                image={item.photo} 
                title={item.enterprise_name} 
                type={item.enterprise_type.enterprise_type_name} 
                country={item.country}
                onClick={() => onClickCard(item)}  
              /> )
            :
            searchedEnterprises !== undefined ?
            <EmptySearchText>Nenhuma empresa foi encontrada para a busca realizada.</EmptySearchText> 
            :
            null
          }
        </ListCards>
        {loading && <Loading/>}
    </Container>
  );
}

export default withRouter(connect(getSearchedEnterprises, (dispatch: any) => ({
  searchEnterprise: (value: string) => dispatch(SearchEnterprises(value)),
  viewEnterprise: (enterprise: Enterprise) => dispatch(ViewEnterprise(enterprise)),
  cleanEnterprise: () => dispatch(CleanSearch()),
}))(Search));

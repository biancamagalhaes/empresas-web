
import React, {useState} from "react";
import {withRouter} from 'react-router-dom';
import {Container, Text} from "./style";
import Input from "../../components/Input";
import Button from "../../components/Button";
import icEmail from "../../assets/ic-email@3x.png";
import icCadeado from "../../assets/ic-cadeado@3x.png";
import { connect } from 'react-redux';
import { SingIn, getInvestor } from '../../ducks/authentication';
import Loading from "../../components/Loading";

export function Login(props: {signInUser: (data: {login: string, password: string}) => any,
    loading: boolean, history: any}) {

  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [error, setError] = useState(false);

  const {signInUser, loading, history} = props;

  const signIn = () => {
    signInUser({login, password}).then(
        (r) => r !== null ? 
        history.push({pathname: "/"}) : 
        setError(true))
  }

  const onChangeValue = (value: string, type: string) => {
    if(error){
        setError(false);
    }  

    if(type === "login"){
        setLogin(value);
    }else if( type === "password"){
        setPassword(value);
    }
  }

  return (
    <Container>
        <Input 
            hasIconRight={false} 
            iconLeft={icEmail} 
            type={"email"} 
            onChange={(value: string) => onChangeValue(value, "login")} 
            placeholder={"E-mail"} 
            error={error}/>
        <Input 
            hasIconRight={true} 
            iconLeft={icCadeado} 
            type={showPassword ? "text" : "password"} 
            onChange={(value: string) => onChangeValue(value, "password")} 
            showPassword={showPassword} 
            onClick={() => setShowPassword(!showPassword)} 
            placeholder={"Senha"} error={error}
            singIn={signIn}/>
        {error && 
        <Text>Credenciais informadas são inválidas, tente novamente.</Text>}
        <Button 
            text={'ENTRAR'} 
            type={'submit'} 
            error={error}
            onClick={signIn}/>
        {loading && <Loading/>}
    </Container>
  );
}

export default withRouter(connect(getInvestor, (dispatch: any) => ({
    signInUser: (data: { login: string; password: string; }) => dispatch(SingIn(data)),
  }))(Login));

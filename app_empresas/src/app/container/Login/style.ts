import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Text = styled.h3`
  margin: 0.75rem 0.438rem 0.625rem 0.938rem;
  font-family: Roboto;
  font-size: 0.76rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.95;
  letter-spacing: -0.17px;
  text-align: center;
  color: #ff0f44;
`;


import React, {useEffect} from "react";
import { Container, Header, ContainerCard } from "./style";
import { withRouter } from "react-router-dom";
import EnterprisesBar from "../../components/EnterprisesBar";
import BigCardEnterprise from "../../components/BigCardEnterprise";
import { connect } from "react-redux";
import { Enterprise, FindEnterprise, getEnterprise } from "../../ducks/enterprise";

export function EnterpriseContainer(props: {enterprise: Enterprise, match: any, findEnterprise: (id: string) => void, searchEnterprise: (value: string) => void, history: any}) {

  const {enterprise, findEnterprise, match, history} = props;

  useEffect(() => {
    if(!enterprise?.id){
      findEnterprise(match.params.id);
    }
  }, [enterprise, findEnterprise, match]);
  
  return (
    <Container>
      <Header>
        <EnterprisesBar title={enterprise.enterprise_name} onClick={() => history.push({pathname: "/search"})}/>
      </Header>
      <ContainerCard>
        <BigCardEnterprise
          image={enterprise.photo}
          description={enterprise.description}
        />
      </ContainerCard>
      
    </Container>
  );
}

export default withRouter(
  connect(getEnterprise, (dispatch: any) => ({
    findEnterprise: (id: string) => dispatch(FindEnterprise(id))
  }))(EnterpriseContainer)
);

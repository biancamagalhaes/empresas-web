import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import Login from "../app/screens/Login";
import Home from "../app/screens/Home";
import Search from "../app/screens/Search";
import Enterprise from "../app/screens/Enterprise";
import { hot } from "react-hot-loader";
import { getCookie } from "../app/utility/cookie";
/*import Loadable from "react-loadable"; */

/* const Login = Loadable({
  loader: () => import('app/screens/Login'),
  loading() {
    return (
      <div
      >
      </div>
    );
  },
}); */

function PrivateRoute({ children, path }) {
  return (
    <Route exact path={path}>
      {getCookie('acessToken') ? children : <Redirect to="/login" />}
    </Route>
  );
}

export const App = hot(module)(() => (
  <Router
    style={{
      flex: 1,
      display: "flex",
    }}
  >
    <Switch>
      <Route exact path="/login">
        <Login />
      </Route>
      <PrivateRoute path="/search" >
        <Search />
      </PrivateRoute>
      <PrivateRoute path="/enterprise/:id" >
        <Enterprise />
      </PrivateRoute>
      <PrivateRoute path="/" >
        <Home />
      </PrivateRoute>
      
    </Switch>
  </Router>
));

import {Container, Country, Image, TextContainer, Title, Type} from "./style";

export function CardEnterprise(props: {onClick: () => void, title: string, country: string, type: string, image: string}) {
  
  const {onClick, title, country, type, image} = props;

  return (
    <Container onClick={onClick}>
      <Image src={`${process.env.REACT_APP_API}${image}`}/>
      <TextContainer>
        <Title>{title}</Title>
        <Type>{type}</Type>
        <Country>{country}</Country>
      </TextContainer>
    </Container>    
  );
}

export default CardEnterprise;

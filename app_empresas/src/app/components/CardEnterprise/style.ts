import styled from "styled-components";

export const Container = styled.button`
  width: 57.69rem;
  height: 13.366rem;
  margin: 2.75rem 3.185rem 0 3.125rem;
  padding: 1.688rem 15.86rem 1.679rem 1.914rem;
  border-radius: 4.7px;
  background-color: #ffff;
  flex-direction: row;
  align-items: center;
  display: flex;
  border: none;
  outline: none;
`;

export const Image = styled.img`
  width: 18.313rem;
  height: 10rem;
  margin: 0 2.397rem 0 0;
  padding: 0 0.04rem 0rem 0;
  object-fit: contain;
`;

export const TextContainer = styled.div`
  margin: 0;
  font-family: GillSans;
  font-size: 1.208rem;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #FFFF;
`;

export const Title = styled.h1`
  margin: 1.438rem 0 0 2.461rem;
  font-family: Roboto;
  font-size: 1.875rem;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #1a0e49;
`;

export const Type = styled.h2`
  width: 10.79rem;
  height: 2rem;
  margin: 0.875rem 8.417rem 0.25rem 2.397rem;
  font-family: Roboto;
  font-size: 1.5rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #8d8c8c;
`;

export const Country = styled(Type)`
  width: 7.483rem;
  height: 1.5rem;
  margin: 0.25rem 11.724rem 2.375rem 2.397rem;
  font-size: 1.125rem;
`;







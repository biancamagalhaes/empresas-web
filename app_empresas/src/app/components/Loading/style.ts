import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex: 1;
  position: absolute;
  background-color: rgba(255, 255, 255, 0.6);
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
`;
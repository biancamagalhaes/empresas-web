import {Container} from "./style";
import { Ring } from 'react-awesome-spinners'

export function Loading() {

  return (
    <Container>
        <Ring size={74} color={'#57bbbc'}/>
    </Container>
  );
}

export default Loading;

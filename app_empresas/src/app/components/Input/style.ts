import styled from "styled-components";
import {EyeFill} from '@styled-icons/bootstrap';
import {Eye} from '@styled-icons/bootstrap';
import {ExclamationCircleFill} from '@styled-icons/bootstrap';

export const Container = styled.div`
  display: flex;
  width: 21.75rem;
  margin: 0.125rem 0 1.063rem;
  border-bottom:  ${props => `solid 0.6px ${props.color}`};
`;

export const IconLeft = styled.img`
  width: 1.625rem;
  height: 1.625rem;
  margin: 0.063rem 0.625rem 0rem 0.063rem;
  padding: 0.438rem 0.375rem 0rem 0.313rem;
  object-fit: contain;
`;

export const Button = styled.button`
  display: flex;
  border: none;
  align-items: center;
  background-color: transparent;
  flex: 0.03;
  outline: none;
`;

export const GreyEyeFill = styled(EyeFill)`
  width: 1.813rem;
  height: 1.25rem;
  color: rgba(0, 0, 0, 0.54);
`
export const GreyEye = styled(Eye)`
  width: 1.813rem;
  height: 1.25rem;
  color: rgba(0, 0, 0, 0.54);
`
export const RedExclamationCircle = styled(ExclamationCircleFill)`
  width: 1.813rem;
  height: 1.25rem;
  color: #ff0f44;
`

export const Input = styled.input`
  width: 14rem;
  margin: 0 1.438rem 0rem 0.625rem;
  font-family: 'Roboto';
  font-size: 1.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -0.31px;
  color: #383743;
  background-color: transparent;
  border: none;
  outline: none;
`;






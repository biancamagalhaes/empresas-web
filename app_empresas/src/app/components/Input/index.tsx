import {Container, IconLeft, Input, Button, GreyEye, GreyEyeFill, RedExclamationCircle} from "./style";

export function InputComponent(props: {error: boolean, iconLeft: string, type: string, 
    onChange: (value: string) => void, placeholder: string, onClick?: () => void, showPassword?: boolean, 
    hasIconRight: boolean, singIn?: () => void}) {
  
  const {error, iconLeft, type, onChange, placeholder, onClick, showPassword, hasIconRight, singIn} = props;

  const onEventKey = (event) => {
    if(event.charCode === 13){
      singIn?.();
    }
  }

  return (
    <Container color={error ? '#ff0f44' : '#383743'}>
        <IconLeft src={iconLeft}/>
        <Input 
            type={type} 
            onChange={(event) => onChange(event.target.value)} 
            placeholder={placeholder} 
            onKeyPress={onEventKey}/>
        {(error || hasIconRight) &&
        <Button onClick={error ? null : onClick}>
            {error ? <RedExclamationCircle/> : showPassword ? <GreyEye/> : <GreyEyeFill/> }
        </Button>}
    </Container>
  );
}

export default InputComponent;

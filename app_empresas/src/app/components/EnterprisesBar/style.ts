import styled from "styled-components";
import {ArrowLeftShort} from '@styled-icons/bootstrap';

export const Container = styled.div`
  display: flex;
  flex: 1;
  height: 2.438rem;
  padding: 1.063rem 2.5rem 1.625rem;
  margin: 0.623rem 0 0;
`;

export const WhiteArrow = styled(ArrowLeftShort)`
  display: flex;
  color: white;
  width: 3.75rem;
  height: 3.75rem;
  align-items: center;
`;

export const Button = styled.button`
  display: flex;
  border: none;
  align-items: center;
  background-color: transparent;
  width: 3.75rem;
  height: 3.75rem;
  outline: none;
`;

export const Text = styled.h1`
  height: 2.5rem;
  margin: 0.625rem 0 0.625rem 4.25rem;
  font-family: Roboto;
  font-size: 2.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #ffff;
`;






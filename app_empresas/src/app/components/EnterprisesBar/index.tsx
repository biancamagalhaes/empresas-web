import {Container, Text, WhiteArrow, Button} from "./style";

export function EnterpriseBar(props: {onClick: () => void, title: string}) {

  const {onClick, title} = props;

  return (
    <Container>
        <Button onClick={onClick}>
          <WhiteArrow/>
        </Button>  
        <Text>{title}</Text>
    </Container>
  );
}

export default EnterpriseBar;

import styled from "styled-components";

export const Container = styled.div`
  flex: 1;
  width: 57.75rem;
  margin: 2.75rem 3.185rem 2.75rem 3.125rem;
  padding: 3.688rem 1.86rem 3.679rem 1.914rem;
  border-radius: 4.8px;
  background-color: #ffff;
  flex-direction: column;
  display: flex;
  align-items: center;
`;

export const Image = styled.img`
  flex: 1;
  display: flex;
  width: 48.5rem;
  object-fit: contain;
`;

export const Description = styled.h2`
  width: 47.625rem;
  margin: 3rem 0.438rem 0 0.563rem;
  font-family: 'Roboto';
  font-size: 2.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: justify;
  color: #8d8c8c;
  flex: 1;
`;







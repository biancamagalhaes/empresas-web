import {Container, Image, Description} from "./style";

export function CardEnterprise(props: {description: string, image: string}) {

  const {description, image} = props;

  return (
    <Container>
      <Image src={`${process.env.REACT_APP_API}${image}`}/>
      <Description>{description}</Description>
    </Container>    
  );
}

export default CardEnterprise;

import styled from "styled-components";
import {Search} from '@styled-icons/bootstrap';
import {X} from '@styled-icons/bootstrap';

export const Container = styled.div`
  display: flex;
  flex: 1;
  justify-content: space-between;
  align-items: flex-end;
  border-bottom: solid 0.6px #ffff;
`;

export const Input = styled.input`
  display: flex;
  flex: 1;
  margin: 0 0.5rem 0.923rem 0;
  font-family: 'Roboto';
  font-size: 1.5rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -0.31px;
  color: #ffff;
  background-color: transparent;
  border: none;
  outline: none;

  ::placeholder {
       color: #991237;
   }
`;

export const Button = styled.button`
  display: flex;
  border: none;
  align-items: center;
  background-color: transparent;
  width: 3.75rem;
  height: 3.75rem;
  padding: 0.813rem 0.881rem 0.944rem 0.875rem;
  flex: 0.025;
  outline: none;
`;

export const WhiteSearch = styled(Search)`
  display: flex;
  color: white;
  flex: 0.02;
  width: 2.75rem;
  height: 2.75rem;
  padding: 0.813rem 0.881rem 0.644rem 0.875rem;
  align-items: center;
`

export const WhiteX = styled(X)`
  color: white;
`





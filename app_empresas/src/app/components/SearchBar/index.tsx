import {useState} from "react";
import {Container, Button, WhiteSearch, Input, WhiteX} from "./style";

export function SearchBar(props: {search: (value: string) => void, type: string, 
  cleanEnterprises: () => void, searchedWord: string}) {

  const {search, type, cleanEnterprises, searchedWord} = props;
  
  const [searchValue, setSearchValue] = useState(searchedWord);

  const onEventText = (event) => {
    setSearchValue(event.target.value)
  }

  const onEventKey = (event) => {
    if(event.charCode === 13){
      search(searchValue);
    }
  }

  const cleanSearch = () => {
    setSearchValue("");
    cleanEnterprises();
  }

  return (
    <Container>
        <WhiteSearch/>
        <Input 
          type={type} 
          value={searchValue} 
          onChange={onEventText} 
          onKeyPress={onEventKey} 
          placeholder={'Pesquisar'}/>
        {searchValue !== "" &&
        <Button onClick={cleanSearch}>
          <WhiteX/>
        </Button>}
    </Container>
  );
}

export default SearchBar;

import {Button, Text} from "./style";

export function ButtonComponent(props: {error: boolean, onClick: () => void, type: string, text: string}) {

  const {error, onClick, type, text} = props;

  return (
      <Button onClick={onClick} type={type} color={error ? '#748383': '#57bbbc'}>
          <Text>{text}</Text>
      </Button>    
  );
}

export default ButtonComponent;

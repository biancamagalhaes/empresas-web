import styled from "styled-components";

export const Button = styled.button`
  width: 21.75rem;
  height: 3.563rem;
  margin: 1.063rem 0 0;
  padding: 1.125rem 8.375rem 1rem 8.438rem;
  border-radius: 3.9px;
  background-color: ${props => props.color};
  border: none;
  align-items: center;
  justify-content: center;
  outline: none;
`;

export const Text = styled.h1`
  margin: 0;
  font-family: GillSans;
  font-size: 1.208rem;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #FFFF;
`;







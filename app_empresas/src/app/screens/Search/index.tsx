import {Container} from "./style";
import SearchComponent from "../../container/Search";

export function Search() {
  return (
    <Container>
        <SearchComponent/>
    </Container>
  );
}

export default Search;

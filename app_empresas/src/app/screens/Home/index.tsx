import {withRouter} from 'react-router-dom'
import {Container, Logo, Header, Text, Button, WhiteSearch} from "./style";
import logoNav from "../../assets/logo-nav@3x.png";

export function Home(props: {history: any}) {

  const {history} = props;

  return (
    <Container>
        <Header>
          <Logo src={logoNav}/>
          <Button onClick={() => history.push({pathname: `/search`})}>
            <WhiteSearch/>
          </Button>
        </Header>
        <Text>Clique na busca para iniciar.</Text>
    </Container>
  );
}

export default withRouter(Home);

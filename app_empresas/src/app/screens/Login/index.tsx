import {Container, BoxMiddle, Logo, WelcomeText, Description} from "./style";
import LogoSrc from '../../assets/logo-home@3x.png';
import LoginContainer from "../../container/Login";

export function Login() {
  return (
    <Container>
        <BoxMiddle>
            <Logo src={LogoSrc}/>
            <WelcomeText>BEM-VINDO AO EMPRESAS</WelcomeText>
            <Description>
                Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
            </Description>
            <LoginContainer/>
        </BoxMiddle>
    </Container>
  );
}

export default Login;

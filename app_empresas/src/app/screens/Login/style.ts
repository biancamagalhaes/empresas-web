import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex: 1;
  margin: -8px;
  background-color: #eeecdb;
  justify-content: center;
  align-items: center;
`;

export const BoxMiddle = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Logo = styled.img`
  width: 18.438rem;
  height: 4.5rem;
  object-fit: contain;
`;

export const WelcomeText = styled.h1`
  color: #383743;
  font-size: 1.6rem;
  letter-spacing: -1.2px;
  width: 10.75rem;
  height: 3.75rem;
  margin: 4.063rem 5.75rem 1.563rem 5.75rem;
  font-family: 'Roboto';
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -1.2px;
  text-align: center;
`;

export const Description = styled.h3`
  width: 21.938rem;
  height: 3.125rem;
  margin: 1.563rem 2.5rem 2.875rem 2.5rem;
  font-family: 'Roboto';
  font-size: 1.063rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.48;
  letter-spacing: 0.2px;
  text-align: center;
  color: #383743;
`;